import Header from "./components/Header";
import Search from "./components/Search";
import Movie from "./components/Movie";

function App() {
  return (
    <div>
      <Header />
      <Search />
      <Movie />
    </div>
  );
}

export default App;

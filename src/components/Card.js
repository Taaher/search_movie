const Card = ({ title, year, id, pic }) => {
  return (
    <div className="card mb-4">
      <img
        className="card-img-top"
        src={pic}
        alt="Card-cap"
        style={{ height: "80%" }}
      />
      <div className="card-body">
        <h5 className="card-title  text-truncate">{title}</h5>
        <p className="card-text">{year}</p>
        <button className="btn btn-outline-dark btn-sm">Download</button>
      </div>
    </div>
  );
};

export default Card;

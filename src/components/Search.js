import { useDispatch } from "react-redux";
import { useEffect, useState } from "react";
import { searchMovie } from "../redux/movie/movieActions";

const Search = () => {
  const [searchFilm, setSearchFilm] = useState("");
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(searchMovie(searchFilm));
  }, [dispatch, searchFilm]);

  return (
    <div className="row mt-2">
      <div className="mt-2 mb-4 col-lg-6 col-md-8 col-sm-12 mx-auto">
        <div className="card">
          <div className="card-body">
            <div className="input-group mb-3 input-group-lg">
              <input
                type="text"
                className="form-control"
                placeholder="Type your name movie"
                name="searchFilm"
                value={searchFilm}
                onChange={(e) => setSearchFilm(e.target.value)}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Search;

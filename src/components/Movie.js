import { useSelector } from "react-redux";
import Card from "./Card";

const Movie = () => {
  const { movies, errorMessage, isLoading } = useSelector(
    (state) => state.movie
  );

  return (
    <>
      {isLoading && !errorMessage ? (
        <span>loading... </span>
      ) : errorMessage ? (
        <div className="errorMessage">{errorMessage}</div>
      ) : (
        <div className="container">
          <div className="row row-cols-1 row-cols-sm-2 row-cols-md-3 row-cols-lg-3 row-cols-xl-4">
            {movies &&
              movies.map((movie) => {
                return (
                  <Card
                    id={movie.imdbID}
                    title={movie.Title}
                    year={movie.year}
                    pic={movie.Poster}
                  />
                );
              })}
          </div>
        </div>
      )}
    </>
  );
};

export default Movie;

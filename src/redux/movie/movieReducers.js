import { SEARCH_MOVIE, SEARCH_MOVIE_FAILED } from "./MovieTypes";

const initialState = {
  movies: [],
  isLoading: true,
  errorMessage: null,
};

export const movieReducers = (state = initialState, action) => {
  switch (action.type) {
    case SEARCH_MOVIE:
      return {
        ...state,
        isLoading: false,
        movies: action.payload,
      };
    case SEARCH_MOVIE_FAILED:
      return {
        ...state,
        isLoading: false,
        errorMessage: action.error,
      };
    default:
      return state;
  }
};

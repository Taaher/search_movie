import { SEARCH_MOVIE, SEARCH_MOVIE_FAILED } from "./MovieTypes";
import axios from "axios";

export const searchMovie = (search) => async (dispatch) => {
  try {
    const movie = await axios.get(
      `https://www.omdbapi.com/?s=${search}&apikey=4a3b711b`
    );
    console.log("search movie", movie);
    dispatch({ type: SEARCH_MOVIE, payload: movie.data["Search"] });
  } catch (error) {
    dispatch({ type: SEARCH_MOVIE_FAILED, payload: error });
  }
};

import { combineReducers, createStore, applyMiddleware } from "redux";
import reduxThunk from "redux-thunk";
import reduxLogger from "redux-logger";
import { movieReducers } from "./movie/movieReducers";

export const configureStore = () => {
  const store = createStore(
    combineReducers({
      movie: movieReducers,
    }),
    applyMiddleware(reduxLogger, reduxThunk)
  );
  return store;
};
